let g:lightline = {
        \   'active': {
        \     'left': [ [ 'mode', 'paste'], [ 'readonly', 'relativepath', 'modified' ] ],
        \   },
        \ }
