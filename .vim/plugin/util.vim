function! PushTagStackOnCommand(command_name) abort
    call s:PushTagStackBefore()
    exec a:command_name
    call s:PushTagStackAfter()
endfunction

function! PushTagStackOnFunction(function_name) abort
    call s:PushTagStackBefore()
    call a:function_name()
    call s:PushTagStackAfter()
endfunction

function! s:PushTagStackBefore() abort
    let dotag = &tagstack && exists('*gettagstack') && exists('*settagstack')
    if dotag
        let from = [bufnr('%'), line('.'), col('.'), 0]
        let tagname = expand('<cword>')
        let stack = gettagstack()
        if stack.curidx > 1
            let stack.items = stack.items[0:stack.curidx-2]
        else
            let stack.items = []
        endif
        let stack.items += [{'from': from, 'tagname': tagname}]
        let stack.curidx = len(stack.items)
        call settagstack(win_getid(), stack)
    endif
endfunction

function! s:PushTagStackAfter() abort
    let dotag = &tagstack && exists('*gettagstack') && exists('*settagstack')
    if dotag
        let curidx = gettagstack().curidx + 1
        call settagstack(win_getid(), {'curidx': curidx})
    endif
endfunction
