let s:todo_items = ['TODO', 'DONE']
let s:todo_items_regex = '\* \(TODO\|DONE\) ::'

function! markdown#todo_items_set(state) abort
    execute 's/' . s:todo_items_regex . '/* ' . a:state . ' ::/'
endfunction

function! markdown#todo_items_set_next() abort
    let l:line = getline('.')

    for i in range(len(s:todo_items))
        if l:line =~# ('.*\* ' . s:todo_items[i] . ' :: .*')
            let l:next_todo_item_index = (i + 1) % len(s:todo_items)
            let l:next_todo_item = s:todo_items[l:next_todo_item_index]
            call markdown#todo_items_set(l:next_todo_item)
        endif
    endfor
endfunction
