function! gxfind#find(args)
    let findcmd = 'find . -type f ' . a:args
    if executable('fd')
        let findcmd = 'fd --hidden --type f ' . a:args . ' . '
    endif

    :cexpr system(findcmd)
endfunction

