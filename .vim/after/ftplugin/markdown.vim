nmap <buffer> j gj
nmap <buffer> k gk

autocmd CursorHold,CursorHoldI <buffer> silent update

setlocal iskeyword+=[,\-

setlocal formatoptions-=t
setlocal formatoptions-=c

nnoremap <silent> <buffer> <localleader>t :call markdown#todo_items_set_next()<cr>
nnoremap <silent> <buffer> <localleader>Td :call markdown#todo_items_set('DONE')<cr>
