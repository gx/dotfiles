let g:jedi#auto_initialization = 0
let g:jedi#auto_vim_configuration = 0
let g:jedi#show_call_signatures = 2
let g:jedi#popup_on_dot = 0

function! s:on_python_filetype() abort
    packadd jedi-vim

    setlocal omnifunc=jedi#completions

    nnoremap <silent> <buffer> <localleader>rn :call jedi#rename()<cr>
    nnoremap <silent> <buffer> gd :call jedi#goto()<cr>
    nnoremap <silent> <buffer> gr :call jedi#usages()<cr>
    nnoremap <silent> <buffer> gy :call jedi#goto_stubs()<cr>
    nnoremap <silent> <buffer> K :call jedi#show_documentation()<cr>

    call jedi#configure_call_signatures()
endfunction

autocmd FileType python call s:on_python_filetype()
