let g:UltiSnipsExpandTrigger='<c-j>'
let g:UltiSnipsJumpForwardTrigger='<c-j>'
let g:UltiSnipsJumpBackwardTrigger='<c-k>'

let g:UltiSnipsSnippetDirectories = [ 'UltiSnips', 'UltiSnips.local' ]

packadd ultisnips
