let g:minpac_initialized = 0

function! InitMinpac() abort
  if g:minpac_initialized
    return
  endif

  if empty(glob('~/.vim/pack/vendor/opt/minpac'))
    silent !git clone https://github.com/k-takata/minpac.git
          \ ~/.vim/pack/vendor/opt/minpac/
  endif

  packadd minpac
  call minpac#init({'package_name': 'vendor'})

  call minpac#add('https://github.com/k-takata/minpac', {'type': 'opt'})

  call minpac#add('https://github.com/lifepillar/vim-mucomplete', {'type': 'opt'})
  call minpac#add('https://github.com/dense-analysis/ale.git', {'type': 'opt'})
  call minpac#add('https://github.com/SirVer/ultisnips.git', {'type': 'opt'})
  call minpac#add('https://github.com/honza/vim-snippets')
  call minpac#add('https://github.com/ctrlpvim/ctrlp.vim.git', {'type': 'opt'})
  call minpac#add('https://github.com/FelikZ/ctrlp-py-matcher.git', {'type': 'opt'})
  call minpac#add('https://github.com/preservim/tagbar.git')
  call minpac#add('https://github.com/puremourning/vimspector', {'type': 'opt'})

  call minpac#add('https://github.com/prabirshrestha/vim-lsp', {'type': 'opt'})
  call minpac#add('https://github.com/rhysd/vim-lsp-ale.git', {'type': 'opt'})

  call minpac#add('https://github.com/andymass/vim-matchup.git', {'type': 'start'})
  call minpac#add('https://github.com/wellle/targets.vim.git', {'type': 'start'})

  call minpac#add('https://github.com/easymotion/vim-easymotion.git', {'type': 'opt'})
  call minpac#add('https://github.com/airblade/vim-rooter.git')
  call minpac#add('https://github.com/mbbill/undotree.git')
  call minpac#add('https://github.com/godlygeek/tabular.git')
  call minpac#add('https://github.com/dhruvasagar/vim-table-mode.git')
  call minpac#add('https://github.com/junegunn/vim-easy-align')
  call minpac#add('https://github.com/romainl/vim-qf.git')
  call minpac#add('https://github.com/tpope/vim-scriptease.git')
  call minpac#add('https://github.com/Konfekt/FastFold', {'type': 'start'})

  " Add end after if, do, def in Ruby.
  " Add endif, endfunction in Vimscript.
  call minpac#add('https://github.com/tpope/vim-endwise', {'type': 'start'})

  call minpac#add('https://github.com/tpope/vim-eunuch.git')
  call minpac#add('https://github.com/tpope/vim-sleuth.git')
  call minpac#add('https://github.com/tpope/vim-unimpaired.git')
  call minpac#add('https://github.com/tpope/vim-surround.git')
  call minpac#add('https://github.com/tpope/vim-rsi.git')
  call minpac#add('https://github.com/tpope/vim-dispatch.git')

  call minpac#add('https://github.com/vim-test/vim-test', {'type': 'start'})

  " external
  call minpac#add('https://github.com/junegunn/fzf.git')
  call minpac#add('https://github.com/junegunn/fzf.vim.git', {'type': 'opt'})

  " terminal
  call minpac#add('https://github.com/voldikss/vim-floaterm.git')

  " git
  call minpac#add('https://github.com/tpope/vim-fugitive.git')
  call minpac#add('https://github.com/rbong/vim-flog.git')
  call minpac#add('https://github.com/mhinz/vim-signify.git')
  call minpac#add('https://github.com/jpalardy/vim-slime', {'type': 'opt'})

  " appearance
  call minpac#add('https://github.com/joshdick/onedark.vim.git', {'type': 'opt'})
  call minpac#add('https://github.com/itchyny/lightline.vim')

  " chinese input method
  call minpac#add('https://github.com/lilydjwg/fcitx.vim.git', {'type': 'opt'})
  call minpac#add('https://github.com/ybian/smartim.git', {'type': 'opt'})

  " language specific
  call minpac#add('https://github.com/habamax/vim-godot.git')
  call minpac#add('https://github.com/ledger/vim-ledger.git', {'type': 'start'})
  call minpac#add('https://github.com/axvr/org.vim.git')
  call minpac#add('https://github.com/chr4/nginx.vim')
  call minpac#add('https://github.com/preservim/vim-markdown')
  call minpac#add('https://github.com/iamcco/markdown-preview.nvim', {
        \ 'type': 'start',
        \ 'do': 'packloadall! | call mkdp#util#install()'})
  call minpac#add('https://github.com/davidhalter/jedi-vim', {'type': 'opt'})
  call minpac#add('https://github.com/rust-lang/rust.vim', {'type': 'start'})
  call minpac#add('https://github.com/vim-ruby/vim-ruby', {'type': 'start'})
  call minpac#add('https://github.com/tpope/vim-rails', {'type': 'start'})
  call minpac#add('https://github.com/tpope/vim-bundler', {'type': 'start'})

  " misc
  call minpac#add('https://github.com/freitass/todo.txt-vim.git', {'type': 'opt'})
  call minpac#add('https://github.com/ntpeters/vim-better-whitespace.git', {'type': 'opt'})
  call minpac#add(
      \ 'https://github.com/igxlin/noteutil', {'type': 'start'})

  let g:minpac_initialized = 1
endfunction

command! MinpacUpdate source $MYVIMRC | call InitMinpac() | call minpac#update()
command! MinpacClean source $MYVIMRC | call InitMinpac() | call minpac#clean()
command! MinpacStatus call InitMinpac() | call minpac#status()
