if executable('fzf')
    finish
endif

let g:ctrlp_map = '<leader>ff'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_use_caching = 0
let g:ctrlp_extensions = ['tag', 'buffertag', 'quickfix', 'line']
let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }

let g:ctrlp_user_command = 'find %s -type f'
if executable('fd')
    let g:ctrlp_user_command = '
                \ fd --hidden --type f . %s
                \ --exclude .git'
endif

nnoremap <leader>fb :CtrlPBuffer<CR>
nnoremap <leader>fm :CtrlPMRU<CR>
nnoremap <leader>fT :CtrlPTag<CR>
nnoremap <leader>ft :CtrlPBufTag %<CR>
nnoremap <leader>fq :CtrlPQuickfix<CR>
nnoremap <leader>fl :CtrlPLine %<CR>

packadd ctrlp.vim
packadd ctrlp-py-matcher
