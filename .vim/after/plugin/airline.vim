let g:airline#extensions#disable_rtp_load = 1
let g:airline_symbols_ascii = 1
let g:airline_extensions = ['tabline', 'ale', 'hunks']

let g:airline_extensions += ['branch']
let g:airline#extensions#branch#format = 2

packadd vim-airline
packadd vim-airline-themes
