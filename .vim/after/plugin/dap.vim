finish

packadd nvim-dap
packadd nvim-dap-ui
packadd nvim-dap-go

lua <<EOF

local dap = require 'dap'
local dap_go = require 'dap-go'

local widgets = require 'dap.ui.widgets'

vim.keymap.set('n', '<localleader>b', dap.toggle_breakpoint)
vim.keymap.set('n', '<localleader>B', dap.list_breakpoints)
vim.keymap.set('n', '<localleader>c', dap.continue)
vim.keymap.set('n', '<localleader>n', dap.step_over)
vim.keymap.set('n', '<localleader>si', dap.step_into)
vim.keymap.set('n', '<localleader>so', dap.step_out)
vim.keymap.set('n', '<localleader>rl', dap.run_last)
vim.keymap.set('n', '<localleader>ut', dap.run_to_cursor)
vim.keymap.set('n', '<localleader>su', dap.up)
vim.keymap.set('n', '<localleader>sd', dap.down)
vim.keymap.set('n', '<localleader>K', widgets.hover)

float_scopes = function ()
    widgets.centered_float(widgets.scopes)
end

float_frames = function ()
    widgets.centered_float(widgets.frames)
end

float_threads = function ()
    widgets.centered_float(widgets.threads)
end

vim.keymap.set('n', '<localleader><localleader>s', float_scopes)
vim.keymap.set('n', '<localleader><localleader>f', float_frames)
vim.keymap.set('n', '<localleader><localleader>t', float_threads)
vim.keymap.set('n', '<localleader><localleader>r', dap.repl.toggle)

dap_go.setup()

EOF

