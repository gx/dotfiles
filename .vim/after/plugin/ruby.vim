let g:rubycomplete_buffer_loading = 0
let g:rubycomplete_classes_in_global = 0
let g:rubycomplete_rails = 0
let g:rubycomplete_load_gemfile = 0
let g:rubycomplete_use_bundler = 0
