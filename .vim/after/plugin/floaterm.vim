let g:floaterm_autoclose = 1
let g:floaterm_width = 0.8
let g:floaterm_height = 0.8
let g:floaterm_opener = 'tabe'

nnoremap <Leader><Leader>t :FloatermToggle<CR>
nnoremap <Leader><Leader>n :FloatermNew --opener=edit nnn<CR>
nnoremap <Leader>gs :FloatermNew --opener=edit tig status<CR>
