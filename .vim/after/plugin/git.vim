if has('nvim')
    let g:gitblame_enabled = 0

    packadd plenary.nvim
    packadd diffview.nvim
    packadd git-blame.nvim
endif

function! UnderGitRepo()
    let status = system("git rev-parse --is-inside-work-tree")
    return v:shell_error == 0
endfunction

function! s:gitfiles_findfunc(cmdarg, cmdcomplete)
    let fnames = systemlist('git ls-files | grep -i ' . a:cmdarg )
    call setqflist([], ' ', { 'efm': '%f', 'lines': fnames, 'nr': '$'})
    return fnames
endfunction

if has('patch-9.1.0831')
    augroup mime
        autocmd BufEnter * call s:set_findfunc()
    augroup end
endif

function! s:set_findfunc()
    if get(b:, 'mime_set_findfunc_finished', 0)
        return
    endif

    if UnderGitRepo()
        setlocal findfunc=s:gitfiles_findfunc
    endif

    let b:mime_set_findfunc_finished = 1
endfunction
