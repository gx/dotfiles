if !has('nvim')
    finish
endif

inoremap <silent><expr> <TAB> pumvisible() ? "\<C-n>" : "\<TAB>"
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

packadd nvim-cmp
packadd cmp-buffer
packadd cmp-path
packadd cmp-cmdline
packadd cmp-omni

" cmp-nvim-ultisnips is kind of buggy. Disable it before investigation.
" packadd cmp-nvim-ultisnips

lua <<EOF

local cmp = require 'cmp'

cmp.setup({
    preselect = cmp.PreselectMode.None,
    -- snippet = {
    --     expand = function(args)
    --         vim.fn["UltiSnips#Anon"](args.body)
    --     end,
    -- },

    sources = cmp.config.sources({
        -- { name = 'ultisnips' },
        { name = 'nvim_lsp' },
        { name = 'omni' },
    }, {
        { name = 'buffer' },
    }),

    mapping = cmp.mapping.preset.insert({
        ['<Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            else
                fallback()
            end
        end, { 'i', 's' }),

        ['<S-Tab>'] = cmp.mapping(function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            else
                fallback()
            end
        end, { 'i', 's' }),
    }),
})

cmp.setup.cmdline('/', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = 'buffer' },
    }),
})

cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
        { name = 'path' },
    }, {
        { name = 'cmdline' },
    }),
})

EOF
