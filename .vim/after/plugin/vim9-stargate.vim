if has('nvim')
    finish
endif

" For 1 character to search before showing hints
noremap <space>f <Cmd>call stargate#OKvim(1)<CR>
" For 2 consecutive characters to search
noremap <space>s <Cmd>call stargate#OKvim(2)<CR>

" for the start of a word
noremap <space>w <Cmd>call stargate#OKvim('\<')<CR>
" for the end of a word
noremap <space>e <Cmd>call stargate#OKvim('\S\>')<CR>
