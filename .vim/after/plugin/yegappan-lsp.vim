packadd yegappan-lsp

if executable('gopls')
	call LspAddServer([#{
		\    name: 'golang',
		\    filetype: ['go', 'gomod'],
		\    omnicompl: v:true,
		\    path: exepath('gopls'),
		\    args: ['serve'],
		\    syncInit: v:true
		\  }])
endif

if executable('solargraph')
	call LspAddServer([#{
		\    name: 'ruby',
		\    filetype: ['ruby'],
		\    omnicompl: v:true,
		\    path: exepath('solargraph'),
		\    args: ['stdio'],
		\    syncInit: v:true
		\  }])
endif

if executable('rust-analyzer')
	call LspAddServer([#{
		\    name: 'rust',
		\    filetype: ['rust'],
		\    omnicompl: v:true,
		\    path: exepath('rust-analyzer'),
		\    args: [],
		\    syncInit: v:true
		\  }])
endif

if executable('clangd')
	call LspAddServer([#{
		\    name: 'clangd',
		\    filetype: ['c', 'cpp'],
		\    omnicompl: v:true,
		\    path: exepath('clangd'),
		\    args: [],
		\    syncInit: v:true
		\  }])
endif

if executable('zk')
    call LspAddServer([#{
        \ name: 'zk',
        \ filetype: ['markdown'],
        \ omnicompl: v:false,
        \ path: exepath('zk'),
        \ args: ['lsp'],
        \ syncInit: v:true,
        \ }])
endif

call LspOptionsSet(#{
	\   aleSupport: v:true,
	\   autoComplete: v:false,
	\   omniComplete: v:true,
	\   useQuickfixForLocations: v:true,
	\ })

function! s:on_lsp_attached() abort
	setlocal keywordprg=:LspHover
	" setlocal formatexpr=lsp#lsp#FormatExpr()

	nnoremap <buffer> gd <Cmd>LspGotoDefinition<CR>
	nnoremap <buffer> <C-W>gd <Cmd>topleft LspGotoDefinition<CR>

	nnoremap <buffer> <localleader>rn :LspRename<CR>
	nnoremap <buffer> gr :LspShowReferences<CR>
	nnoremap <buffer> gI <Cmd>LspGotoImpl<CR>

	autocmd! BufWritePre *.rs,*.go call execute('LspFormat')
	autocmd! BufRead *.go,*.rs setlocal tagfunc=lsp#lsp#TagFunc
endfunction

augroup lsp_attached
	au!
	autocmd User LspAttached call s:on_lsp_attached()
augroup END
