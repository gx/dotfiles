if has('nvim')
    finish
endif

if executable('gtags-cscope')
    let &csprg='gtags-cscope'
endif

set cscopequickfix=g-,s-,c-,d-,i-,t-,e-
set cscopetag
set cscoperelative

nmap <leader>css :cs find s <C-R>=expand("<cword>")<CR><CR>
nmap <leader>csg :cs find g <C-R>=expand("<cword>")<CR>
nmap <leader>csc :cs find c <C-R>=expand("<cword>")<CR>
nmap <leader>cst :cs find t <C-R>=expand("<cword>")<CR><CR>
nmap <leader>cse :cs find e <C-R>=expand("<cword>")<CR><CR>
nmap <leader>csf :cs find f <C-R>=expand("<cfile>")<CR><CR>
nmap <leader>csi :cs find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nmap <leader>csd :cs find d <C-R>=expand("<cword>")<CR>
nmap <leader>csa :cs find a <C-R>=expand("<cword>")<CR>

nmap <leader>cs<space> :cs find<space>

if has('cscope')
    let tagfile = findfile("GTAGS", ".;")
    if filereadable(tagfile)
        exec "cs add" tagfile
    endif
endif
