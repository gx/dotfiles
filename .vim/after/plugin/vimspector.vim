packadd vimspector

nmap <leader>di <plug>VimspectorBalloonEval
xmap <leader>di <plug>VimspectorBalloonEval

nmap <leader>db <plug>VimspectorToggleBreakpoint
nmap <leader>dB <plug>VimspectorBreakpoints

nmap <leader>dc <plug>VimspectorContinue
nmap <leader>dr <plug>VimspectorRstart
nmap <leader>dn <plug>VimspectorStepOver
nmap <leader>dsi <plug>VimspectorStepInto
nmap <leader>dso <plug>VimspectorStepOut

nmap <leader>dut <plug>VimspectorRunToCursor
nmap <leader>dq :VimspectorReset<CR>

