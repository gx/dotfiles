if has('nvim')
    finish
endif

let g:mucomplete#enable_auto_at_startup = 1
let g:mucomplete#minimum_prefix_length = 1
let g:mucomplete#empty_text_auto = 1
let g:mucomplete#no_mappings = 1
let g:mucomplete#always_use_completeopt = 1

let g:mucomplete#can_complete = {}
let s:default_cond = { t -> strlen(&omnifunc) > 0 && t =~# '\%(\.\)$' }
let g:mucomplete#chains = {}
let g:mucomplete#chains.default =
      \ [ 'ulti', 'user', 'omni', 'c-p', 'path' ]

let s:c_cond = { t -> strlen(&omnifunc) > 0 && t =~# '\%(\.\|->\)$' }
let g:mucomplete#can_complete.c = { 'omni': s:c_cond }
let g:mucomplete#can_complete.objc = { 'omni': s:c_cond }

let s:cpp_cond = { t -> strlen(&omnifunc) > 0 && t =~# '\%(\.\|->\|::\)$' }
let g:mucomplete#can_complete.cpp = { 'omni': s:cpp_cond }
let g:mucomplete#can_complete.objcpp = { 'omni': s:cpp_cond }
let g:mucomplete#can_complete.cuda = { 'omni': s:cpp_cond }
let g:mucomplete#can_complete.ruby = { 'omni': s:cpp_cond }
let g:mucomplete#can_complete.rust = { 'omni': s:cpp_cond }

let s:ledger_cond = { t -> t =~# '\%(:\)$' }
let g:mucomplete#can_complete.ledger = { 'omni': s:ledger_cond }

packadd vim-mucomplete
