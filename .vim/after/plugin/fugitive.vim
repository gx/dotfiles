" nnoremap <silent> <leader>gs :tabnew +Git<CR>

nnoremap <leader>gp :G push<space>
nnoremap <leader>g<space> :G<space>
nnoremap <leader>gd :Gvdiffsplit<space>
nnoremap <leader>gl :Gclog %<space>
xnoremap <leader>gl :Gclog<CR>
