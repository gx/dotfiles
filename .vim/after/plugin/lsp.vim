finish
if has('nvim')
    finish
endif

let g:lsp_diagnostics_enabled = 0
let g:lsp_document_code_action_signs_enabled = 0
let g:lsp_use_native_client = 1
let g:lsp_fold_enabled = 1

packadd vim-lsp

" This package will help enable diagnostic config of vim-lsp and then it
" can help send the result to ALE. Pay attention of the loading order.
packadd vim-lsp-ale

if executable('pylsp')
    au User lsp_setup call lsp#register_server({
        \ 'name': 'pylsp',
        \ 'cmd': {server_info->['pylsp']},
        \ 'allowlist': ['python'],
        \ })
endif

if executable('gopls')
    au User lsp_setup call lsp#register_server({
          \ 'name': 'gopls',
          \ 'cmd': {server_info->['gopls']},
          \ 'allowlist': ['go', 'gomod'],
          \ 'initialization_options': {
            \ 'ui.completion.usePlaceholders': v:true,
          \ },
          \ })
endif

if executable('clangd')
    au User lsp_setup call lsp#register_server({
                \ 'name': 'clangd',
                \ 'cmd': {server_info->['clangd']},
                \ 'allowlist': ['c', 'cpp'],
                \ })
endif

if executable('omnisharp')
    au User lsp_setup call lsp#register_server({
                \ 'name': 'omnisharp',
                \ 'cmd': {server_info->['omnisharp', '-lsp']},
                \ 'allowlist': ['cs'],
                \ })
endif

if executable('godot')
    au User lsp_setup
        \ call lsp#register_server({
            \ 'name': 'godot',
            \ 'tcp': {server_info->'localhost:6008'},
            \ 'allowlist': ['gdscript', 'gdscript3']
        \ })
endif

" https://github.com/mickael-menu/zk
" if executable('zk')
"     au User lsp_setup call lsp#register_server({
"                 \ 'name': 'zk',
"                 \ 'cmd': {server_info->['zk', 'lsp']},
"                 \ 'allowlist': ['markdown'],
"                 \ })
" endif

if executable('rust-analyzer')
    au User lsp_setup call lsp#register_server({
                \ 'name': 'rust-analyzer',
                \ 'cmd': {server_info->['rust-analyzer']},
                \ 'allowlist': ['rust'],
                \ })
endif

if executable('typescript-language-server')
    au User lsp_setup call lsp#register_server({
                \ 'name': 'typescript-language-server',
                \ 'cmd': {server_info->['typescript-language-server', '--stdio']},
                \ 'allowlist': ['javascript', 'typescript', 'typescriptreact', 'typescript.tsx'],
                \ })
endif

function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal nocscopetag
    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol)
    nmap <buffer> gS <plug>(lsp-workspace-symbol)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gy <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> K <plug>(lsp-hover)

    let g:lsp_format_sync_timeout = 1000
    autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
endfunction

augroup lsp_install
    au!
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup END
