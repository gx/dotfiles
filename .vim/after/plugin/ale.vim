let g:ale_linters_explicit = 0
let g:ale_echo_msg_format = '[%linter%] %code: %%s'
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:ale_sign_error = 'E>'
let g:ale_sign_warning = 'W>'
let g:ale_hover_cursor = 0
let g:ale_virtualtext_cursor = 1

autocmd FileType markdown let b:ale_languagetool_options = '--autoDetect --disable WHITESPACE_RULE'

nmap <silent> [g <Plug>(ale_previous_wrap)
nmap <silent> ]g <Plug>(ale_next_wrap)

packadd ale
