let g:tagbar_autoclose = 1
let g:tagbar_autofocus = 1

nnoremap <leader><leader>s :TagbarToggle<CR>

let g:tagbar_type_go = {
            \ 'ctagstype': 'go',
            \ 'kinds': [
                \ 'p:package',
                \ 'i:imports',
                \ 'c:constants',
                \ 'v:variables',
                \ 't:types',
                \ 'n:interfaces',
                \ 'w:fields',
                \ 'e:embedded',
                \ 'm:methods',
                \ 'r:constructor',
                \ 'f:functions'
            \ ],
            \ 'sro': '.',
            \ 'kind2scope': {
                \ 't': 'ctype',
                \ 'n': 'ntype'
            \ },
            \ 'scope2kind': {
                \ 'ctype': 't',
                \ 'ntype': 'n'
            \ }
        \ }
