if !executable('fzf')
  finish
endif

let s:fzf_default_opts = $FZF_DEFAULT_OPTS

function! s:build_quickfix_list(lines) abort
  call setqflist(map(copy(a:lines), '{ "filename": v:val }'))
  copen
  cc
endfunction

function! s:format_qf_line(line) abort
  let parts = split(a:line, ':')
  return {
        \ 'filename': parts[0],
        \ 'lnum': parts[1],
        \ 'col': parts[2],
        \ 'text': join(parts[3:], ':')
        \ }
endfunction

function! s:fzf_qf(key, line) abort
  let filepath = expand('#' . a:line.bufnr . ':p')
  return filepath . ':' . a:line.lnum . ':' . a:line.col . ':' . a:line.text
endfunction

" TODO make it accept multiple lines
function! s:fzf_qf_sink(line) abort
  let line = s:format_qf_line(a:line[0])
  execute 'edit +' . line.lnum . ' ' . line.filename
endfunction

let s:command_git_main_branch = 'git symbolic-ref --short refs/remotes/origin/HEAD'
let s:command_git_patch_files_changed = 'git diff'
      \ . ' --name-only'
      \ . ' --relative'
      \ . ' --merge-base $('
      \ . s:command_git_main_branch . ')'

command! FzfQuickFix call fzf#run({
      \ 'source': map(getqflist(), function('s:fzf_qf')),
      \ 'down': 20,
      \ 'sink*': function('s:fzf_qf_sink'),
      \ 'options': s:fzf_default_opts. ' --multi --prompt "QuickFix> "'
      \ })

let g:fzf_command_prefix = 'Fzf'

command! -bang -nargs=* FzfRg
  \ call fzf#vim#grep(
  \   'rg --hidden --column --line-number --no-heading --color=always --smart-case --glob !.git ' . <q-args>, 1,
  \   fzf#vim#with_preview(), <bang>0)

command! FzfGitChangedFiles call fzf#run(fzf#wrap({
      \ 'source': s:command_git_patch_files_changed,
      \ 'sink': 'e'
      \ }))

noremap <leader>ff :FzfFiles<CR>
noremap <leader>fgf :FzfGitChangedFiles<CR>
noremap <leader>fb :FzfBuffers<CR>
noremap <leader>fm :FzfHistory<CR>
noremap <leader>ft :FzfBTags<CR>
noremap <leader>fT :FzfTags<CR>
noremap <leader>fr :FzfRg<space>
noremap <leader>rg :FzfRg<space>
noremap <leader>fc :FzfCommands<CR>
noremap <leader>fgc :FzfBCommits<CR>
noremap <leader>fgC :FzfCommits<CR>
noremap <leader>fl :FzfBLines<CR>
noremap <leader>fL :FzfLines<CR>
noremap <leader>fq :FzfQuickFix<CR>

let g:fzf_action = {
      \ 'ctrl-q': function('s:build_quickfix_list')
      \ }
let g:fzf_layout = { 'down': '40%' }
let g:fzf_preview_window = [ 'right:80%:hidden', 'ctrl-/' ]

function! s:set_fzffiles_mapping()
  if get(b:, 'mime_set_fzffiles_mapping_finished', 0)
    return
  endif

  if UnderGitRepo()
    noremap <buffer> <leader>ff :FzfGFiles<cr>
  endif

  let b:mime_set_fzffiles_mapping_finished = 1
endfunction

augroup mime
  autocmd BufEnter * call s:set_fzffiles_mapping()
augroup END

packadd fzf.vim
