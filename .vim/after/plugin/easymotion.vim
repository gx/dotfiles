let g:EasyMotion_do_mapping = 0
let g:EasyMotion_smartcase = 1
let g:EasyMotion_smartsign_us = 1
" let g:EasyMotion_startofline = 0

map <space>j <Plug>(easymotion-j)
nmap <space>j <Plug>(easymotion-j)

map <space>k <Plug>(easymotion-k)
nmap <space>k <Plug>(easymotion-k)

map <space>l <Plug>(easymotion-lineforward)
nmap <space>l <Plug>(easymotion-lineforward)

map <space>h <Plug>(easymotion-linebackward)
nmap <space>h <Plug>(easymotion-linebackward)

map <space><space> <Plug>(easymotion-bd-jk)
nmap <space><space> <Plug>(easymotion-overwin-line)

map <space>n <Plug>(easymotion-bd-n)
nmap <space>n <Plug>(easymotion-bd-n)

nmap <space>/ <Plug>(easymotion-bd-fn)

nmap <space>; <Plug>(easymotion-repeat)

packadd vim-easymotion
