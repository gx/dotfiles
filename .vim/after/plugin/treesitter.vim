if !has('nvim')
    finish
endif

packadd nvim-treesitter

lua <<EOF

local treesitter_config = require 'nvim-treesitter.configs'

treesitter_config.setup {
    sync_install = false,

    highlight = {
        enable = true,
    },
}

EOF
