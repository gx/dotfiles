if executable('rg')
    set grepprg=rg\ --vimgrep\ --smart-case\ --hidden\ --glob\ !.git
    set grepformat=%f:%l:%c:%m
endif

nnoremap <leader><leader>g :grep! <c-r>=expand('<cword>')<cr><cr>

if maparg('<leader>fr', 'n') == ''
    nnoremap <leader>fr :grep!<space>
endif
