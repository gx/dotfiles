finish

command! NoteutilToday call noteutil#open(
            \ 'journal --date today')
command! NoteutilTodayWeekly call noteutil#open(
            \ 'journal --date today --period weekly')
command! NoteutilYesterdayWeekly call noteutil#open(
            \ 'journal --date yesterday --period weekly')

command! NoteutilBacklinks call noteutil#backlinks({'jump': v:true})

autocmd FileType markdown setlocal omnifunc=noteutil#complete
