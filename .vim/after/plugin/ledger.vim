let g:ledger_bin = 'hledger'
let g:ledger_is_hledger = v:true

let g:ledger_align_at = 50
let g:ledger_maxwidth = 80
let g:ledger_commodity_before = 0
let g:ledger_align_commodity = 1
let g:ledger_align_last = v:true
let g:ledger_decimal_sep = '.'
let g:ledger_commodity_sep = ' '
let g:ledger_date_format = '%Y-%m-%d'
let g:ledger_extra_options = ''
let g:ledger_main = expand('$LEDGER_FILE')

function! HLedgerEntry() abort
  let l:output = systemlist(join(['hledger', '--no-conf', 'print', '--match', getline('.')]))
  " Filter out warnings
  let l:output = filter(l:output, "v:val !~? '^Warning: '")
  " Errors may occur
  if v:shell_error
    echomsg join(l:output)
    return
  endif
  " Append output so we insert instead of overwrite, then delete line
  call append('.', l:output)
  normal! "_dd
endfunction

function! s:ledger_keymap() abort
    noremap [[ ?^\d<CR>
    noremap ]] /^\d<CR>

    noremap <silent> <buffer> * :call ledger#transaction_state_toggle(line('.'), ' *!')<CR>
    nnoremap <silent> <buffer> <Tab> vip!hledger -f- print<CR>
    inoremap <silent> <buffer> <c-x><c-x> <c-o>:call HLedgerEntry()<CR>
endfunction

au FileType ledger call s:ledger_keymap()
