let g:vim_markdown_new_list_item_indent = 2
let g:vim_markdown_folding_disabled = 0

let g:vim_markdown_frontmatter = 1
