if !has('nvim')
    finish
endif

packadd hop.nvim

noremap <space>f :HopChar1<CR>
noremap <space>s :HopChar2<CR>

noremap <space>w :HopWord<CR>

lua <<EOF
    require('hop').setup()
EOF
