" fcitx.vim
if !empty(glob('/usr/bin/fcitx5'))
  if executable('fcitx5-remote')
    let g:fcitx5_remote = 'fcitx5-remote'
  endif

  packadd fcitx.vim
endif

" smartim
if has('mac')
  packadd smartim
  let g:smartim_default = 'com.apple.keylayout.ABC'
endif
