if !has('nvim')
    finish
endif

packadd cscope_maps.nvim

nmap <leader>css :Cscope find s <C-R>=expand("<cword>")<CR><CR>
nmap <leader>csg :Cscope find g <C-R>=expand("<cword>")<CR>
nmap <leader>csc :Cscope find c <C-R>=expand("<cword>")<CR>
nmap <leader>cst :Cscope find t <C-R>=expand("<cword>")<CR><CR>
nmap <leader>cse :Cscope find e <C-R>=expand("<cword>")<CR><CR>
nmap <leader>csf :Cscope find f <C-R>=expand("<cfile>")<CR><CR>
nmap <leader>csi :Cscope find i ^<C-R>=expand("<cfile>")<CR>$<CR>
nmap <leader>csd :Cscope find d <C-R>=expand("<cword>")<CR>
nmap <leader>csa :Cscope find a <C-R>=expand("<cword>")<CR>
nmap <leader>cs<space> :Cscope find<space>
nmap <C-]> :Cstag <C-R>=expand('<cword>')<CR>

lua <<EOF

    local cscope_maps = require 'cscope_maps'
    cscope_maps.setup({
        disable_maps = true,
        cscope = {
            exec = "gtags-cscope",
        },
    })

EOF
