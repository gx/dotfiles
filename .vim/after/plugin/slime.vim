let g:slime_no_mappings = 1

xmap <leader>gz <Plug>SlimeRegionSend
nmap <leader>gz <Plug>SlimeLineSend

let g:slime_target = 'vimterminal'
let g:slime_vimterminal_config = 
      \{'term_finish': 'close'}

autocmd FileType python let g:slime_vimterminal_cmd = 'python3'

packadd vim-slime
