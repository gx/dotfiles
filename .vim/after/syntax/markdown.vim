syntax match mkdListItemCheckbox
            \ /\%(TODO\|DOING\|DONE\)\ze\s\+/
            \ contained contains=mkdListItem
