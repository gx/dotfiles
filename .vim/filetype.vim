if exists('did_load_filetypes')
    finish
endif

augroup filetypedetect
    autocmd BufRead,BufNewFile *.org setfiletype org
    autocmd BufRead,BufNewFile *nginx.conf setfiletype nginx
    autocmd BufRead,BufNewFile /etc/nginx*.conf setfiletype nginx
    autocmd BufRead,BufNewFile *mutt-* setfiletype mail

    autocmd BufRead,BufNewFile .vimspector.json 
                \   setfiletype vimspector
                \ | setlocal syntax=json
augroup end

autocmd FileType html setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType css setlocal shiftwidth=2 softtabstop=2 expandtab
autocmd FileType javascript setlocal shiftwidth=2 softtabstop=2 expandtab
