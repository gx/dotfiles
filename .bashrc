#
# ~/.bashrc
#

source "$HOME/.macos"
source "$HOME/.env"
source "$HOME/.path"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PS1='[\u@\h \W]\$ '

source "$HOME/.aliases"
source "$HOME/.fzfrc"

[ -r "$HOME/.bash_local" ] && source "$HOME/.bash_local"

if command -v fish > /dev/null; then
    __FISH_IS_ACTIVE="$__FISH_IS_ACTIVE"
    if [[ -z "$BASH_EXECUTION_STRING" && -z "$__FISH_IS_ACTIVE" ]]
    then
        shopt -q login_shell && LOGIN_OPTION='--login' || LOGIN_OPTION=''
        exec env __FISH_IS_ACTIVE=1 SHELL="$(which fish)" fish $LOGIN_OPTION
    fi
    unset __FISH_IS_ACTIVE
fi
