(setq yas-snippet-dirs
      '("~/.emacs.d/snippets"
        "~/.emacs.d/local/snippets"))

(when (gx/maybe-require-package 'yasnippet)
  (gx/maybe-require-package 'yasnippet-snippets)
  (require 'yasnippet)
  (yas-reload-all)
  (add-hook 'prog-mode-hook #'yas-minor-mode)
  (add-hook 'org-mode-hook #'yas-minor-mode))

(provide 'init-snippet)
