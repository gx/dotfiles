(when (gx/maybe-require-package 'ledger-mode)
  (add-to-list 'auto-mode-alist '("\\.journal\\'" . ledger-mode)))
(with-eval-after-load 'ledger-mode
  (setq ledger-default-date-format ledger-iso-date-format))

(gx/maybe-require-package 'realgud)

(setq plantuml-jar-path "~/.local/bin/plantuml.jar"
      plantuml-default-exec-mode 'jar
      org-plantuml-jar-path plantuml-jar-path
      org-plantuml-default-exec-mode 'jar)
(when (gx/maybe-require-package 'plantuml-mode)
  (add-to-list 'auto-mode-alist '("\\.plantuml\\'" . plantuml-mode))
  (with-eval-after-load 'plantuml
    (unless (file-readable-p plantuml-jar-path)
	(plantuml-download-jar)))
  (with-eval-after-load 'org
    (add-to-list 'org-src-lang-modes '("plantuml" . plantuml))))

(provide 'init-lang)
