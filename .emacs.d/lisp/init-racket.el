(when (gx/maybe-require-package 'racket-mode)
  (add-to-list 'auto-mode-alist '("\\.rkt\\'" . racket-mode)))

(provide 'init-racket)
