(when (gx/maybe-require-package 'magit)
  (define-key global-map (kbd "C-x g") #'magit-status))

(setq magit-display-buffer-function #'magit-display-buffer-fullframe-status-v1)

(with-eval-after-load 'evil
  (evil-define-key 'normal 'global
    (kbd "<leader>gs") #'magit-status))

(setq magit-refresh-status-buffer nil)

(when (gx/maybe-require-package 'git-gutter)
  (autoload #'global-git-gutter-mode "git-gutter" nil t)
  (add-hook 'after-init-hook #'global-git-gutter-mode 'append))

(with-eval-after-load 'git-gutter
  (gx/diminish 'git-gutter-mode))

(with-eval-after-load 'evil
  (evil-define-key 'normal 'git-gutter-mode
    (kbd "]c") 'git-gutter:next-hunk
    (kbd "[c") 'git-gutter:previous-hunk
    (kbd "<leader>gh") 'git-gutter:popup-hunk))

(provide 'init-git)
