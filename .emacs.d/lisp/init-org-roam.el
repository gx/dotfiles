;; roam

(when (gx/maybe-require-package 'org-roam)
  (define-key global-map (kbd "C-c n f") #'org-roam-node-find)
  (define-key global-map (kbd "C-c n c") #'org-roam-capture)
  (define-key global-map (kbd "C-c n j") #'org-roam-dailies-capture-today)
  (with-eval-after-load 'org
    (define-key org-mode-map (kbd "C-c n i") #'org-roam-node-insert)
    (define-key org-mode-map (kbd "C-c n l") #'org-roam-buffer-toggle)
    (define-key org-mode-map (kbd "C-c n s") #'org-roam-db-sync)))

(with-eval-after-load 'evil
  (evil-define-key 'normal 'global
    (kbd "<leader>fn") 'org-roam-node-find
    (kbd "<leader>cn") 'org-roam-dailies-goto-today)
  (evil-define-key 'normal org-mode-map
    (kbd "<leader><SPC>b") 'org-roam-buffer-toggle
    (kbd "<leader>in") 'org-roam-node-insert
    (kbd "[d") 'org-roam-dailies-goto-previous-note
    (kbd "]d") 'org-roam-dailies-goto-next-note))

(setq org-roam-directory (file-truename "~/notes"))
(setq org-roam-db-gc-threshold most-positive-fixnum)
(setq org-roam-dailies-directory "journals/")
(setq org-roam-dailies-capture-templates
      '(("d" "daily" entry
	 "* %?"
	 :target (file+head "%<%Y/%Y-%m-%d>.org"
			    "#+title: %<%a %d %b %Y>\n"))
	("w" "weekly" entry
	 "* %?"
	 :target (file+head+olp "%<%Y/week%V>.org"
			        "#+title: Week %<%V %Y>\n"
                                ("%<%Y-%m-%d %A>")))
	("m" "monthly" entry
	 "* %?"
	 :target (file+head "%<%Y/%Y-%m>.org"
			    "#+title: %<%B %Y>\n"))
	("y" "yearly" entry
	 "* %?"
	 :target (file+head "%<%Y/%Y>.org"
			    "#+title: %<%Y>\n"))))
(setq org-roam-node-display-template "${title} - ${file}")

;; roam ui
(when (gx/maybe-require-package 'org-roam-ui)
  (define-key global-map (kbd "C-c n g") #'org-roam-ui-open))
(setq org-roam-ui-follow t
      org-roam-ui-sync-theme t
      org-roam-ui-update-on-save t
      org-roam-ui-open-at-start t)

(provide 'init-org-roam)
