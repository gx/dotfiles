;; -*- lexical-binding: t -*-

(when (gx/maybe-require-package 'vertico)
  (autoload #'vertico-mode "vertico")
  (add-hook 'after-init-hook #'vertico-mode 'append))

(setq vertico-resize t
      vertico-count 20
      vertico-cycle t)

(setq completion-ignore-case t
      read-file-name-completion-ignore-case t
      read-buffer-completion-ignore-case t)
(with-eval-after-load 'vertico
  (define-key vertico-map (kbd "C-c j") 'vertico-quick-jump)
  (define-key vertico-map (kbd "C-c C-j") 'vertico-quick-jump)
  (define-key vertico-map (kbd "C-d")
              (lambda () (interactive) (vertico-next (/ vertico-count 2))))
  (define-key vertico-map (kbd "C-u")
              (lambda () (interactive) (vertico-previous (/ vertico-count 2)))))

(when (gx/maybe-require-package 'consult)
  (when (gx/maybe-require-package 'projectile)
    (autoload 'projectile-project-root "projectile")
    (setq consult-project-function (lambda (_) (projectile-project-root))))
  (with-eval-after-load 'evil
    (evil-define-key 'normal 'global
      (kbd "<leader>fb") 'consult-buffer
      (kbd "<leader>fm") 'consult-recent-file
      (kbd "<leader>fc") 'execute-extended-command
      (kbd "<leader>fl") 'consult-line
      (kbd "<leader>fs") 'consult-imenu
      (kbd "<leader>fa") 'consult-org-agenda)
    (evil-define-key 'normal org-mode-map
      (kbd "<leader>fh") 'consult-org-heading)
    (evil-define-key 'normal 'eglot--managed-mode
      (kbd "<leader>fS") 'consult-eglot-symbols)))

(when (executable-find "rg")
  (setq affe-grep-command
        "rg --null --no-line-buffered --color=never --max-columns=1000 \
--path-separator / -v ^$ \
--smart-case --no-heading --line-number --hidden --glob !.git ."))

(when (executable-find "fd")
  (setq affe-find-command
        "fd --type f --hidden --follow --exclude .git"))

(setq affe-count most-positive-fixnum)
(when (gx/maybe-require-package 'affe)
  (autoload 'affe-find "affe")
  (autoload 'affe-grep "affe")
  (with-eval-after-load 'evil
    (evil-define-key 'normal 'global
      (kbd "<leader>ff") 'affe-find
      (kbd "<leader>fr") 'affe-grep))
  (with-eval-after-load 'orderless
    (defun gx/affe-orderless-regexp-compiler (input _type _ignorecase)
      (setq input (orderless-pattern-compiler input))
      (cons input (lambda (str) (orderless--highlight input str))))
    (setq affe-regexp-compiler #'gx/affe-orderless-regexp-compiler))
  (with-eval-after-load 'consult
    (consult-customize
     affe-grep affe-find
     :preview-key (kbd "C-c C-p"))))

(setq consult-preview-key (kbd "C-c C-p"))

(when (gx/maybe-require-package 'savehist)
  (add-hook 'after-init-hook #'savehist-mode 'append))

(provide 'init-minibuffer)
