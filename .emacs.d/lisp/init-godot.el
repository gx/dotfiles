(when (gx/maybe-require-package 'gdscript-mode)
  (gx/maybe-require-package 'hydra)
  (add-to-list 'auto-mode-alist '("\\.tscn\\'" . gdscript-mode))
  (add-to-list 'auto-mode-alist '("\\.gd\\'" . gdscript-mode))
  (when (gx/maybe-require-package 'eglot)
    (add-hook 'gdscript-mode-hook #'eglot-ensure)))

(setq gdscript-use-tab-indents nil
      gdscript-indent-offset 4)

(provide 'init-godot)
