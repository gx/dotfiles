(when (gx/maybe-require-package 'markdown-mode)
  (add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))
  (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
  (add-to-list 'auto-mode-alist '("\\.markdown\\'" . markdown-mode)))

(with-eval-after-load 'evil
  (evil-define-key 'normal markdown-mode-map
    (kbd "<RET>") 'markdown-follow-link-at-point))

(setq markdown-enable-wiki-links t
      markdown-wiki-link-search-type '(project)
      markdown-wiki-link-alias-first nil
      markdown-enable-math t)

(setq markdown-command '("pandoc" "--from=markdown" "--to=html5"))

(provide 'init-markdown)
