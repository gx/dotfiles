(when (gx/maybe-require-package 'go-mode)
  (autoload #'go-mode "go-mode" nil t)
  (add-to-list 'auto-mode-alist '("\\.go\\'" . go-mode)))

(add-hook 'go-mode-hook (lambda () (setq tab-width 4)))

(gx/maybe-require-package 'go-dlv)

(when (gx/maybe-require-package 'eglot)
  (add-hook 'go-mode-hook #'gx/eglot-format-before-save)
  (add-hook 'go-mode-hook #'gx/eglot-code-action-organize-imports-before-save)
  (add-hook 'go-mode-hook #'eglot-ensure))

(provide 'init-go)
