(gx/maybe-require-package 'vterm)

(setq vterm-kill-buffer-on-exit t)

(provide 'init-terminal)
