(when (gx/maybe-require-package 'eglot)
  (add-hook 'c-mode-common-hook #'eglot-ensure))

(provide 'init-c)
