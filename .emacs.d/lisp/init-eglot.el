(when (gx/maybe-require-package 'eglot)
  (autoload #'eglot-ensure "eglot" nil t))

(defun gx/eglot-format-before-save ()
  (add-hook 'before-save-hook #'eglot-format nil t))

(defun gx/eglot-code-action-organize-imports-before-save ()
  (add-hook 'before-save-hook #'eglot-code-action-organize-imports nil t))

(with-eval-after-load 'evil
  (evil-define-key 'normal 'eglot--managed-mode
    (kbd "gd") #'xref-find-definitions
    (kbd "gr") #'xref-find-references
    (kbd "gt") #'eglot-find-typeDefinition
    (kbd "K") #'eldoc-doc-buffer
    (kbd "gi") #'eglot-find-implementation
    (kbd "C-t") #'xref-pop-marker-stack

    (kbd "<leader>rn") #'eglot-rename))

(setq eglot-autoreconnect t)

(with-eval-after-load 'eglot
  (setq completion-category-defaults nil)
  (setq read-process-output-max (* 1024 1024)))

(provide 'init-eglot)
