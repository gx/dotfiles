(when (gx/maybe-require-package 'company)
  (add-hook 'org-mode-hook #'gx/company-mode-in-tui)
  (add-hook 'ledger-mode-hook #'gx/company-mode-in-tui)
  (add-hook 'prog-mode-hook #'gx/company-mode-in-tui))

(defun gx/company-mode-in-tui ()
  "Enable company mode in TUI."
  (when (not (display-graphic-p))
    (company-mode)))

(with-eval-after-load 'company
  (require 'company-tng)
  (company-tng-configure-default)
  (gx/diminish 'company-mode))

(setq company-minimum-prefix-length 1
      company-idle-delay (lambda () (if (company-in-string-or-comment) nil 0.01))
      company-selection-wrap-around t)

(provide 'init-company)
