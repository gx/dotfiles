(when (gx/maybe-require-package 'orderless)

  (require 'orderless)

  (setq orderless-matching-styles
        '(orderless-literal orderless-regexp orderless-prefixes))

  (defun gx/orderless-without-if-bang (pattern _index _total)
    (cond
     ((equal "!" pattern)
      '(orderless-literal . ""))
     ((string-prefix-p "!" pattern)
      `(orderless-without-literal . ,(substring pattern 1)))))

  (defun gx/orderless-flex-if-twiddle (pattern _index _total)
    (when (string-suffix-p "~" pattern)
      `(orderless-flex . ,(substring pattern 0 -1))))

  (setq orderless-style-dispatchers
        '(gx/orderless-without-if-bang
          gx/orderless-flex-if-twiddle))

  (setq completion-styles '(substring orderless flex)
        completion-category-defaults nil
        completion-category-overrides
        '((file (styles . (partial-completion))))))

(when (gx/maybe-require-package 'cape)
  (autoload #'cape-file "cape")
  (add-to-list 'completion-at-point-functions #'cape-file))

(provide 'init-completion)
