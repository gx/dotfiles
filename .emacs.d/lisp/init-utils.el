(defun gx/diminish (mode &optional to-what)
  (when (gx/maybe-require-package 'diminish)
    (require 'diminish)
    (diminish mode to-what)))

(setq exec-path-from-shell-arguments '("-l"))
(when (gx/maybe-require-package 'exec-path-from-shell)
  (when (memq window-system '(mac ns))
    (require 'exec-path-from-shell)
    (exec-path-from-shell-initialize)))

(provide 'init-utils)
