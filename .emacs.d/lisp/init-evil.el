(when (gx/maybe-require-package 'evil)
  (autoload #'evil-mode "evil")
  (add-hook 'after-init-hook (lambda () (evil-mode 1)) 'append))

(setq evil-want-integration t
      evil-want-C-u-scroll t
      evil-wannt-fine-undo t
      evil-want-keybinding nil
      evil-disable-insert-state-bindings t
      evil-toggle-key "C-x C-z")

(with-eval-after-load 'evil
  (evil-set-leader 'normal ",")
  (evil-set-undo-system 'undo-tree)
  (evil-define-key 'insert 'global
    (kbd "C-r") 'evil-paste-from-register
    (kbd "C-【") 'evil-normal-state)

  (evil-define-key 'normal 'global
    (kbd "gj") 'evil-next-visual-line
    (kbd "gk") 'evil-previous-visual-line

    (kbd "gd") 'xref-find-definitions
    (kbd "gr") 'xref-find-references
    (kbd "C-t") 'xref-pop-marker-stack)
   
  (evil-define-key 'normal text-mode-map
    (kbd "j") 'evil-next-visual-line
    (kbd "k") 'evil-previous-visual-line
    (kbd "gj") 'evil-next-line
    (kbd "gk") 'evil-previous-line)
  (evil-define-key 'visual text-mode-map
    (kbd "j") 'evil-next-visual-line
    (kbd "k") 'evil-previous-visual-line
    (kbd "gj") 'evil-next-line
    (kbd "gk") 'evil-previous-line)
  (gx/diminish 'evil-mode))
  
;; evil-collection
(when (gx/maybe-require-package 'evil-collection)
  (autoload 'evil-collection-init "evil-collection")
  (with-eval-after-load 'evil
    (evil-collection-init
     '(xref
       dired
       ibuffer
       image
       magit))))

(with-eval-after-load 'evil-collection-unimpaired
  (gx/diminish 'evil-collection-unimpaired-mode))

(provide 'init-evil)
