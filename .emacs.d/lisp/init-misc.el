(setq tab-always-indent 'complete
      completion-cycle-threshold t)

(setq scroll-conservatively most-positive-fixnum)

(when (version<= "26.0.50" emacs-version)
  (global-display-line-numbers-mode))
(setq vc-follow-symlinks t)
(setq find-file-visit-truename t)
(setq enable-recursive-minibuffers t)

;; Disable evil operation writes clipboard
(setq x-select-enable-clipboard nil)

(setq inhibit-splash-screen t)

(setq
 backup-by-copying t
 delete-old-versions t
 kept-new-versions 10
 kept-old-versions 2
 version-control t
 backup-directory-alist `((".*" . ,(concat user-emacs-directory "backups"))))

(setq auto-save-file-name-transforms
      `((".*" "~/.emacs.d/backups/" t)))

(setq-default fill-column 72)
(setq-default indent-tabs-mode nil)
(setq-default
 recentf-max-saved-items 1024
 recentf-exclude `("/tmp/"))
(setq show-paren-when-point-inside-paren t
      show-paren-when-point-in-periphery t)

(add-hook 'prog-mode-hook #'hs-minor-mode) ;; allow folding
(add-hook 'after-init-hook
	  (lambda ()
	    (global-hl-line-mode)
	    (menu-bar-mode -1)
	    (xterm-mouse-mode 1) ;; Enable mouse
	    (tool-bar-mode -1)
            (scroll-bar-mode -1)
            ;; Auto reload buffers whenever they are updated externally
	    (global-auto-revert-mode t)
	    (global-so-long-mode 1) ;; Disable some modes when the files are so long
	    (recentf-mode)
	    (show-paren-mode)
	    (fset 'yes-or-no-p 'y-or-n-p)
            ;; Show column number in modeline
            (column-number-mode)
            ;; Show the size of file in modeline
            (size-indication-mode)
            (transient-mark-mode t))
          'append)

(with-eval-after-load 'hideshow
  (gx/diminish 'hs-minor-mode))

(gx/maybe-require-package 'rg)
(gx/maybe-require-package 'esup)

(when (gx/maybe-require-package 'which-key)
  (autoload 'which-key-mode "which-key")
  (add-hook 'after-init-hook 'which-key-mode 'append))
(with-eval-after-load 'which-key
  (gx/diminish 'which-key-mode))

(when (gx/maybe-require-package 'undo-tree)
  (autoload 'global-undo-tree-mode "undo-tree")
  (add-hook 'after-init-hook 'global-undo-tree-mode 'append))
(with-eval-after-load 'undo-tree
  (gx/diminish 'undo-tree-mode))
(setq undo-tree-auto-save-history t
      undo-tree-history-directory-alist `(("." . ,(concat user-emacs-directory "undo"))))

(setq eldoc-echo-area-use-multiline-p nil)
(setq eldoc-idle-delay 0.1)
(with-eval-after-load 'eldoc
    (gx/diminish 'eldoc-mode))

(with-eval-after-load 'evil
  (evil-define-key 'normal 'flymake-mode
    (kbd "[g") 'flymake-goto-prev-error
    (kbd "]g") 'flymake-goto-next-error))

(add-hook 'after-init-hook #'winner-mode 'append)
(with-eval-after-load 'evil
  (evil-define-key 'normal 'global
    (kbd "C-w u") 'winner-undo
    (kbd "C-w C-u") 'winner-undo
    (kbd "C-w U") 'winner-redo
    (kbd "C-w C-U") 'winner-redo))

(add-hook 'after-init-hook #'save-place-mode 'append)

(when (gx/maybe-require-package 'edwina)
  (setq display-buffer-base-action '(display-buffer-below-selected))
  (setq edwina-mfact 0.5)
  (add-hook 'after-init-hook #'edwina-mode 'append))

(provide 'init-misc)
