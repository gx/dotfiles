(when *is-a-linux*
  (setq fcitx-remote-command "fcitx5-remote")
  (when (gx/maybe-require-package 'fcitx)
    (add-hook 'after-init-hook 'fcitx-aggressive-setup 'append)))

(when *is-a-mac*
  (setq sis-external-ism "/usr/local/bin/macism"
        sis-english-source "com.apple.keylayout.ABC"
  	sis-other-source "com.apple.inputmethod.SCIM.ITABC")
  (when (gx/maybe-require-package 'sis)
    (sis-ism-lazyman-config sis-english-source sis-other-source)
    (sis-global-respect-mode t)
    (sis-global-context-mode t)
    (sis-global-inline-mode t)

    (add-hook 'evil-insert-state-exit-hook #'sis-set-english)))

(provide 'init-input-method)
