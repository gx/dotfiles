;;; init-package.el -*- lexical-binding: t -*-

(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("mepla" . "https://melpa.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")))
(setq package-user-dir
      (expand-file-name "elpa" user-emacs-directory))

(defun gx/require-package (package &optional min-version no-refresh)
  "Install given PACKAGE, optionally requiring MIN-VERSION.  If
NO-REFRESH is non-nil, the available package lists will not be
re-downloaded in order to locate PACKAGE.

This function is copied from
https://github.com/purcell/emacs.d/blob/master/lisp/init-elpa.el"
  (or (package-installed-p package min-version)
      (let* ((known (cdr (assoc package package-archive-contents)))
             (best (car (sort known (lambda (a b)
                                      (version-list-<= (package-desc-version b)
                                                       (package-desc-version a)))))))
        (if (and best (version-list-<= min-version (package-desc-version best)))
            (package-install best)
          (if no-refresh
              (error "No version of %s >= %S is available" package min-version)
            (package-refresh-contents)
            (gx/require-package package min-version t)))
        (package-installed-p package min-version))))

(defun gx/maybe-require-package (package &optional min-version no-refresh)
  "Try to install PACKAGE, and return non-nil if successful.
In the event of failure, return nil and print a warning message.
Optionally require MIN-VERSION.  If NO-REFRESH is non-nil, the
available package lists will not be re-downloaded in order to
locate PACKAGE.

This function is copied from
https://github.com/purcell/emacs.d/blob/master/lisp/init-elpa.el"
  ;; Autoloading generation will create wrong path in Emacs 28. Set
  ;; find-file-visit-truename nil as a temporary workaround.
  ;; TODO clean up after it is fixed by upstream.
  (let ((find-file-visit-truename nil))
    (condition-case err
        (gx/require-package package min-version no-refresh)
      (error
       (message "Couldn't install optional package `%s': %S" package err)
       nil))))

(setq package-enable-at-startup nil)
(setq package-native-compile t)
(package-initialize)

(gx/maybe-require-package 'auto-package-update)
(setq auto-package-update-delete-old-versions t
      auto-package-update-hide-results t)

(provide 'init-package)
