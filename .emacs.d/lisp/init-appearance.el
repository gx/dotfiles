(defun gx/set-appearance--linux ()
  (set-face-attribute 'default nil :height 120))

(defun gx/set-appearance--mac ()
  (set-face-attribute 'default nil :height 160)
  (custom-set-variables
   '(initial-frame-alist '((fullscreen . maximized)))))

(defun gx/set-appearance-for-frame (frame)
  (select-frame frame)
  (gx/set-appearance))

(defun gx/set-appearance ()
  (when (display-graphic-p)
    (when *is-a-linux*
      (gx/set-appearance--linux))
    (when *is-a-mac*
      (gx/set-appearance--mac)))
  (gx/load-theme))

(defun gx/load-theme ()
  (when (gx/maybe-require-package 'doom-themes)
    (load-theme 'doom-one t)
    (doom-themes-org-config)))

(add-to-list 'after-make-frame-functions #'gx/set-appearance-for-frame)
(add-hook 'after-init-hook #'gx/set-appearance 'append)

(setq-default custom-safe-themes t)

(provide 'init-appearance)
