(defun gx/org-refile (fn &optional arg default-buffer rfloc msg)
  (funcall-interactively fn arg default-buffer rfloc msg)
  (org-save-all-org-buffers))
(advice-add 'org-refile :around 'gx/org-refile)

(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-capture-after-finalize-hook 'org-save-all-org-buffers)

(setq org-image-actual-width '(400))

;; evil
(when (gx/maybe-require-package 'evil-org)
  (autoload #'evil-org-mode "evil-org" nil t)
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'org-agenda-mode-hook 'evil-org-mode))
(with-eval-after-load 'evil-org
  (evil-org-set-key-theme '(navigation insert textobjects additional calendar todo))
  (gx/diminish 'evil-org-mode))

;; keybinds
(with-eval-after-load 'evil
  (evil-define-key 'normal 'global
    (kbd "<leader>a") 'org-agenda
    (kbd "<leader>l") 'org-store-link
    (kbd "<leader>C") 'org-capture)
  (evil-define-key 'normal org-mode-map
    (kbd "TAB") 'org-cycle
    (kbd "RET") 'org-open-at-point
    (kbd "H") 'org-toggle-heading

    (kbd "<leader>r") 'org-refile

    ;; toggle
    (kbd "<leader><SPC>l") 'org-toggle-link-display

    ;; set
    (kbd "<leader>ss") 'org-schedule
    (kbd "<leader>sd") 'org-deadline
    (kbd "<leader>sp") 'org-set-property
    (kbd "<leader>st") 'org-set-tags-command
    (kbd "<leader>se") 'org-set-effort

    ;; insert
    (kbd "<leader>il") 'org-insert-link
    (kbd "<leader>ih") 'org-i
    (kbd "<leader>ii") 'org-id-get-create

    ;; clock
    (kbd "<leader>ci") 'org-clock-in
    (kbd "<leader>co") 'org-clock-out
    (kbd "<leader>cr") 'org-clock-report
    (kbd "<leader>cc") 'org-clock-cancel

    ;; priority
    (kbd "+") 'org-priority-up
    (kbd "-") 'org-priority-down))

(add-hook 'org-mode-hook
	  (lambda ()
	    (setq truncate-lines nil)
	    (setq tab-width 4)))

(setq org-default-notes-file "~/notes/inbox.org")
(setq org-agenda-start-on-weekday 0)

;; appearance
(setq org-src-tab-acts-natively t)
(setq org-adapt-indentation nil)
(setq org-startup-indented t)
(setq org-descriptive-links t)
(setq org-tags-column (- fill-column))
(setq org-agenda-tags-column org-tags-column)
(setq org-startup-folded 'showall)
(setq org-stuck-projects
      '("/+TODO={TODO|WAITING}" ("NEXT") nil "SCHEDULED:"))

(with-eval-after-load 'org-indent
  (gx/diminish 'org-indent-mode))

;; todo
(setq org-todo-keywords
    '((sequence "TODO(t)" ; to do later
		"NEXT(n)" ; doing now or to do soon
		"WAITING(w)" "SOMEDAY(s)"
		"|" "DONE(d)" "CANCELLED(c)")))
(setq org-log-done t)
(setq org-log-into-drawer t)

;; tags
(setq org-tags-exclude-from-inheritance
    '("PROJECT"))
(setq org-tag-alist '((:startgroup . nil)
		      ("@work" . ?w) ("@home" . ?h) ("errants" . ?e)
		      (:endgroup . nil)
		      ("phone" . ?m)
		      ("FLAGGED" . ?f)
		      ("URGENT" . ?u)
		      ("UNWILLING" . ?x)
		      ("PROJECT" . ?p)
		      ("REFILE" . ?r)))

;; refile
(setq org-refile-targets '((nil :maxlevel . 9)
			   (org-agenda-files :maxlevel . 9))
      org-outline-path-complete-in-steps nil
      org-refile-use-outline-path 'file)

;; capture
(setq org-capture-templates
      `(("t" "TODO" entry
         (file+headline org-default-notes-file
                        ,(format-time-string "%Y-%m-%d %A"))
	 "* TODO %?\nCaptured on %U")
        ("j" "Journal" entry
         (file+headline org-default-notes-file
                        ,(format-time-string "%Y-%m-%d %A"))
	 "* %?\nCaptured on %U")
        ("p" "Protocol" entry
         (file+headline org-default-notes-file
                        ,(format-time-string "%Y-%m-%d %A"))
         "* [[%:link][%:description]]\nCaptured on %U\n\n#+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n%?")
        ("L" "Protocol Link" entry
         (file+headline org-default-notes-file
                        ,(format-time-string "%Y-%m-%d %A"))
         "* [[%:link][%:description]]\nCaptured on %U\n%?")))

;; clock
(setq org-clock-persist 'history)
(with-eval-after-load 'org
  (org-clock-persistence-insinuate))

;; modules
(with-eval-after-load 'org
  (add-to-list 'org-modules 'org-habit t)
  (add-to-list 'org-modules 'org-tempo t))
(setq org-confirm-babel-evaluate nil)

;; babel
(with-eval-after-load 'org
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((shell . t)
     (plantuml . t))))

;; alert
(when (gx/maybe-require-package 'org-alert)
  (autoload 'org-alert-enable "org-alert"))
(when *is-a-linux*
  (setq alert-default-style 'libnotify))
(when *is-a-mac*
  (setq alert-default-style 'osx-notifier))
(setq org-alert-interval 60)

;; org-protocol
(add-hook 'after-init-hook
          (lambda ()
	    (require 'server)
            (unless (server-running-p) (require 'org-protocol)))
          'append)

(require 'init-org-roam)
(require 'init-org-agenda)

(provide 'init-org)
