(when (gx/maybe-require-package 'deft)
    (define-key global-map (kbd "C-c n d") #'deft))

(setq deft-recursive t
    deft-use-filter-string-for-filename t
    deft-extensions '("org" "txt" "md" "tex")
    deft-directory org-roam-directory
    deft-use-filename-as-title t)

(provide 'init-deft)
