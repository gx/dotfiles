(when (gx/maybe-require-package 'eglot)
  (add-hook 'python-mode-hook #'eglot-ensure))

;; TODO Check if the venv-directory exists.
;; Raise error message if it doesn't.
(defun gx/python-load-venv ()
  ""
  (setenv "WORKON_HOME" (projectile-project-root))
  (let* ((project-root (projectile-project-root))
         (venv-directory "venv"))
    (setenv "WORKON_HOME" project-root)
    (pyvenv-workon venv-directory)))
  

(when (gx/maybe-require-package 'pyvenv)
  (autoload #'pyvenv-activate "pyvenv" nil t)
  (autoload #'pyvenv-workon "pyvenv" nil t)
  (add-hook 'python-mode-hook #'gx/python-load-venv))

(provide 'init-python)
