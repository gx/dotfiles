;; -*- lexical-binding: t; -*-

(when (gx/maybe-require-package 'corfu)
  (add-hook 'gdscript-mode-hook #'gx/enable-corfu-only-in-gui)
  (add-hook 'ledger-mode-hook #'gx/enable-corfu-only-in-gui)
  (add-hook 'prog-mode-hook #'gx/enable-corfu-only-in-gui))

(defun gx/enable-corfu-only-in-gui ()
  "Enable Corfu Only In GUI mode."
  (when (display-graphic-p)
    (corfu-mode)))

(setq corfu-cycle t
      corfu-auto t
      corfu-quit-no-match t
      corfu-preselect-first nil
      corfu-auto-prefix 1
      corfu-auto-delay 0.01)

(with-eval-after-load 'corfu
  (define-key corfu-map (kbd "TAB") 'corfu-next)
  (define-key corfu-map (kbd "<tab>") 'corfu-next)
  (define-key corfu-map (kbd "S-TAB") 'corfu-previous)
  (define-key corfu-map (kbd "<backtab>") 'corfu-previous))

(provide 'init-corfu)
