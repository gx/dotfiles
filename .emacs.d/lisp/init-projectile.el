(when (gx/maybe-require-package 'projectile)
  (autoload 'projectile-mode "projectile")
  (add-hook 'after-init-hook #'projectile-mode 'append))

(with-eval-after-load 'projectile
  (gx/diminish 'projectile-mode))

(setq projectile-cache-file (expand-file-name ".cache/projectile" user-emacs-directory)
      projectile-require-project-root nil)

(with-eval-after-load 'evil
  (evil-define-key 'normal projectile-mode-map
    (kbd "<leader>fp") 'projectile-switch-project))

(provide 'init-projectile)
