(setq avy-background t
      avy-all-windows t
      avy-timeout-seconds 0.3)
(when (gx/maybe-require-package 'avy)
  (define-key global-map (kbd "C-:") #'avy-goto-char-timer)
  (define-key global-map (kbd "C-'") #'avy-goto-char-2)
  (define-key global-map (kbd "C-c C-j") #'avy-resume)
  (with-eval-after-load 'evil
    (require 'avy)
    (evil-define-key 'normal 'global
      (kbd "<SPC>j") 'evil-avy-goto-line-below
      (kbd "<SPC>k") 'evil-avy-goto-line-above
      (kbd "<SPC>w") 'evil-avy-goto-word-0
      (kbd "<SPC>f") 'evil-avy-goto-char)
    (evil-define-key 'visual 'global
      (kbd "<SPC>j") 'evil-avy-goto-line-below
      (kbd "<SPC>k") 'evil-avy-goto-line-above)
    (evil-define-key 'operator 'global
      (kbd "<SPC>j") 'evil-avy-goto-line-below
      (kbd "<SPC>k") 'evil-avy-goto-line-above)))
 
(provide 'init-avy)
