(gx/maybe-require-package 'org-ql)

(defvar gx/org-agenda-block--today-schedule
  '(agenda "" ((org-agenda-span 1)
	       (org-agenda-overriding-header "Today's Schedule")))
  "A block showing today's schedule.")

(defvar gx/org-agenda-block--weekly-log
  '(agenda "" ((org-agenda-overriding-header "Weekly Log")
	       (org-agenda-span 7)
	       (org-agenda-show-log t)
	       (org-agenda-log-mode-items '(state clock closed))))
  "A block showing my schedule and logged tasks for this week.")

(defvar gx/org-agenda-block--unwilling-to-do
  '(tags-todo "UNWILLING"
	      ((org-agenda-overriding-header "Unwilling to do")))
  "A block showing tasks I am not willing to do ...")

(defvar gx/org-agenda-block--week-at-a-glance
  '(agenda ""
	   ((org-agenda-span 7)
	    (org-agenda-repeating-timestamp-show-all t)
	    (org-agenda-entry-types '(:deadline :scheduled))
	    (org-agenda-overriding-header "Week at a Glance")))
  "A block showing the tasks scheduled or due this week.")

(defvar gx/org-agenda-block--attention
  '(org-ql-block '(and (not (todo "WAITING"))
		       (or (tags "URGENT" "FLAGGED")
			   (priority "A"))
		       (not (done)))
		 ((org-ql-block-header "ATTENTION")))
  "A block showing tasks require attention.")

(defvar gx/org-agenda-block--waiting-not-scheduled
  '(org-ql-block '(and (todo "WAITING")
		       (not (scheduled)))
		 ((org-ql-block-header "Waiting unscheduled tasks")))
  "A block showing waiting and not scheduled tasks.")

(defvar gx/org-agenda-block--next-not-scheduled
  '(org-ql-block '(and (todo "NEXT")
		       (not (scheduled)))
		 ((org-ql-block-header "Next unscheduled tasks")))
  "A block showing next unscheduled tasks.")

(defvar gx/org-agenda-block--inbox
  '(tags "INBOX"
         ((org-agenda-overriding-header "INBOX:")
          (org-tags-match-list-sublevels nil)))
  "A block showing my tasks in INBOX.")

(defvar gx/org-agenda-block--display-settings
  '((org-agenda-todo-ignore-deadlines 'near)
    (org-agenda-todo-ignore-scheduled t))
  "Display settings for all agenda views.")

(defun gx/org-agenda-quit (fn)
  (org-save-all-org-buffers)
  (funcall fn))
(advice-add 'org-agenda-quit :around 'gx/org-agenda-quit)

(with-eval-after-load 'evil
  (evil-set-initial-state 'org-agenda-mode 'motion)
  (evil-define-key 'motion org-agenda-mode-map

    ;; motion
    (kbd "j") 'org-agenda-next-line
    (kbd "k") 'org-agenda-previous-line
    (kbd "gg") 'evil-goto-first-line
    (kbd "G") 'evil-goto-line
    (kbd "C-n") 'org-agenda-next-item
    (kbd "C-p") 'org-agenda-previous-item
    (kbd "[[") 'org-agenda-earlier
    (kbd "]]") 'org-agenda-later

    ;; actions
    (kbd "RET") 'org-agenda-switch-to
    (kbd "TAB") 'org-agenda-goto
    (kbd "t") 'org-agenda-todo
    (kbd "r") 'org-agenda-refile
    (kbd "+") 'org-agenda-priority-up
    (kbd "-") 'org-agenda-priority-down
    (kbd "H") 'org-agenda-do-date-earlier
    (kbd "L") 'org-agenda-do-date-later
    (kbd "o") 'org-agenda-add-note
    (kbd "A") 'org-agenda-append-agenda
    (kbd "C") 'org-agenda-capture
    (kbd "R") 'org-agenda-redo

    (kbd "dd") 'org-agenda-kill

    ;; bulk
    (kbd "m") 'org-agenda-bulk-toggle
    (kbd "x") 'org-agenda-bulk-action

    ;; set
    (kbd "ss") 'org-agenda-schedule
    (kbd "sd") 'org-agenda-deadline
    (kbd "st") 'org-agenda-set-tags
    (kbd "sT") 'org-timer-set-timer
    (kbd "se") 'org-agenda-set-effort

    ;; filter
    (kbd "fc") 'org-agenda-filter-by-category
    (kbd "fr") 'org-agenda-filter-by-regexp
    (kbd "fe") 'org-agenda-filter-by-effort
    (kbd "ft") 'org-agenda-filter-by-tag
    (kbd "F") 'org-agenda-filter-remove-all

    ;; clock
    (kbd "ci") 'org-agenda-clock-in
    (kbd "co") 'org-agenda-clock-out
    (kbd "cc") 'org-agenda-clock-cancel
    (kbd "cg") 'org-agenda-clock-goto
    (kbd "cr") 'org-agenda-clockreport-mode

    ;; goto
    (kbd "gc") 'org-agenda-goto-calendar

    ;; display
    (kbd "vE") 'org-agenda-entry-text-mode

    (kbd "u") 'org-agenda-undo))

(setq org-agenda-dim-blocked-tasks nil)
(setq org-agenda-skip-deadline-prewarning-if-scheduled t)
(setq org-deadline-warning-days 30)
(setq org-agenda-window-setup 'current-window)
;; (setq org-agenda-prefix-format
;; 	'((agenda . "%i %-12:c%?-12t% s %b")
;; 	  (timeline . "  % s")
;; 	  (todo . "%i %-12:c %b")
;; 	  (tags . "%i %-12:c")
;; 	  (search . " %i %-12:c")))
(setq org-agenda-custom-commands
    `((" " "Daily agenda and all important TODOs"
       (,gx/org-agenda-block--today-schedule
        ,gx/org-agenda-block--inbox
	,gx/org-agenda-block--attention
	,gx/org-agenda-block--unwilling-to-do
	;; (tags-todo "computer|@office|phone")
	;; (tags "PROJECT+CATEGORY=\"elephants\"")
	,gx/org-agenda-block--next-not-scheduled
	,gx/org-agenda-block--waiting-not-scheduled)
       ,gx/org-agenda-block--display-settings)
      ("l" "Weekly Log"
       (,gx/org-agenda-block--weekly-log)
       ,gx/org-agenda-block--display-settings)
      ("d" "Upcoming deadlines" agenda ""
       ((org-agenda-time-grid nil)
	(org-deadline-warning-days 365)
	(org-agenda-entry-types '(:deadline))))
      ("g" . "GTD contexts")
      ("gh" "Home" tags-todo "home")
      ("p" . "Priorities")
      ("pa" "A items" tags-todo "+PRIORITIES=\"A\"")
      ("pb" "B items" tags-todo "+PRIORITIES=\"B\"")
      ("pc" "C items" tags-todo "+PRIORITIES=\"C\"")))

(provide 'init-org-agenda)
