;; -*- coding: utf-8; lexical-binding: t; -*-

;; Make startup faster by reducing frequency of garbage collection.
(setq gc-cons-percentage 0.6)
(setq gc-cons-threshold most-positive-fixnum)
;; Make gc pauses faster by decreasing the threhold to 8 MiB (default is
;; 800kB)
(add-hook 'emacs-startup-hook
	  (lambda () (setq gc-cons-threshold (expt 2 23))))

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

(defconst *is-a-mac* (eq system-type 'darwin))
(defconst *is-a-linux* (eq system-type 'gnu/linux))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))

(require 'init-package)
(require 'init-utils)
(require 'init-evil)
(require 'init-avy)
(require 'init-completion)
(require 'init-minibuffer)
(require 'init-company)
(require 'init-corfu)
(require 'init-org)
(require 'init-lang)
(require 'init-go)
(require 'init-python)
(require 'init-c)
(require 'init-eglot)
(require 'init-racket)
(require 'init-deft)
(require 'init-input-method)
(require 'init-projectile)
(require 'init-markdown)
(require 'init-git)
(require 'init-terminal)
(require 'init-misc)
(require 'init-godot)
(require 'init-snippet)
(require 'init-appearance)

(add-hook 'after-init-hook
	  (lambda ()
	    (require 'server)
	    (unless (server-running-p) (server-start)))
          'append)

(load custom-file)

(require 'init-local nil t)

(provide 'init)
