[[ ! -f "$HOME/.config/zsh/powerlevel10k.zsh" ]] || source "$HOME/.config/zsh/powerlevel10k.zsh"

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -e

# Edit command line
autoload -U edit-command-line
zle -N edit-command-line
bindkey '^x^e' edit-command-line

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

MACOS_CONF="$HOME/.config/zsh/macos.zsh"
[[ ! -f "$MACOS_CONF" ]] || source "$MACOS_CONF"

[ -r "$HOME/.path" ] && source $HOME/.path
[ -r "$HOME/.env" ] && source $HOME/.env
[ -r "$HOME/.aliases" ] && source $HOME/.aliases
[ -r "$HOME/.functions" ] && source $HOME/.functions

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

[ -r $HOME/.nnnrc ] && source $HOME/.nnnrc

source "$HOME/.config/zsh/fzf.zsh"

command -v zoxide > /dev/null && eval "$(zoxide init --cmd j zsh)"
[ -d "$HOME/.winevm/shims" ] && export PATH="$HOME/.winevm/shims:$PATH"

if command -v direnv > /dev/null; then
    eval "$(direnv hook zsh)"
fi

if command -v navi > /dev/null; then
    eval "$(navi widget zsh)"
fi

if command -v lazygit > /dev/null; then
    alias lg='lazygit'
fi

if command -v atuin > /dev/null; then
    eval "$(atuin init zsh --disable-up-arrow)"
fi

source ~/.config/zsh/yazi.sh

setopt histignorealldups sharehistory hist_ignore_space

if [[ -d ~/.local/share/zsh-completions ]]; then
    fpath=("$HOME/.local/share/zsh-completions/src" $fpath)
    autoload -Uz compinit; compinit
fi

zstyle ':completion:*' menu select
zstyle ':completion:*:*:*:*:descriptions' format '%F{green}-- %d --%f'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' file-list all
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' squeeze-slashes true

ZSH_AUTOSUGGEST_STRATEGY=(history completion)
if command -v atuin > /dev/null; then
    ZSH_AUTOSUGGEST_STRATEGY=(atuin completion)
fi
SOURCE_ZSH_AUTOSUGGEST="$HOME/.local/share/zsh-autosuggestions/zsh-autosuggestions.zsh"
[[ ! -f "$SOURCE_ZSH_AUTOSUGGEST" ]] || source "$SOURCE_ZSH_AUTOSUGGEST"

[[ ! -d "$HOME/.local/share/zsh-syntax-highlighting" ]] \
    || source "$HOME/.local/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"

[ -r $HOME/.zsh_local ] && source $HOME/.zsh_local
[ -r $HOME/.config/zsh/local.zsh ] && source $HOME/.config/zsh/local.zsh
