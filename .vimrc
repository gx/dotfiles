" General Config

set nocompatible
set number
set backspace=indent,eol,start
syntax enable
set encoding=utf-8
set fileencodings=ucs=bom,utf-8,utf-16,gbk,big5,gb18030,latin1
set showcmd
set autoread
set mouse=a
filetype on
filetype indent on
filetype plugin on
set modeline
set textwidth=72
set formatoptions+=mB
set formatoptions-=tc
set secure
set exrc
set noerrorbells
set noshowmode
set updatetime=400

" use for populating filename in quickfix
set errorformat+=%f

" used by nvim and nvim-yarp
let g:python3_host_prog = '/usr/bin/python3'
if has('macunix')
    let g:python3_host_prog = '/usr/local/bin/python3'
endif

" Use python3 first
if has('python3')
elseif has('python')
endif

if has('patch-9.0.1921')
    set jumpoptions=stack
endif

" GUI

set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions+=c
if has('nvim')
    set guifont=Dejavu\ Sans\ Mono:h12
else
    set guifont=DejaVu\ Sans\ Mono\ 12
endif
if has('macunix')
    set guifont=Monaco:h15
endif

set foldenable
set foldlevelstart=99

set hidden

set completeopt=menu,menuone,noinsert,noselect
if !has('nvim')
    set completeopt+=popup,popuphidden
    set completepopup=align:menu,border:off,highlight:WildMenu
endif
set complete=.,w,b,u " Don't use tags and included files as there may be lots of files
set shortmess+=c
set belloff+=ctrlg

" diff
set diffopt+=followwrap,algorithm:patience,closeoff
if has('patch-9.1.1009')
    set diffopt+=linematch:60
endif

" undo
let s:undodir = expand('~/.vim/.undo')
if has('nvim')
    " The undo file of vim and nvim is not compatible
    let s:undodir = expand('~/.vim/.undo/nvim')
endif
if !isdirectory(expand(s:undodir))
    silent! call mkdir(expand(s:undodir, 'p'))
endif
let &undodir=s:undodir
set undofile

" set Vim-specific sequences for RGB colors
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
set t_Co=256

if has("termguicolors")
    set termguicolors
endif

" Correct background rendering in some terminal
let &t_ut=''

" Identation

set autoindent
set tabstop=4
set shiftwidth=4
set expandtab
set softtabstop=4

set wrap
"set nowrap
"set wrapmargin=4
set scrolloff=5
"set sidescrolloff=5
" always show statline
set laststatus=2
set ruler

set hlsearch
set incsearch
set ignorecase
set smartcase

" Completion

set path+=**
set wildmenu
set wildignorecase
set wildmode=list:longest,full
set wildignore=*.o,*.obj,*~
set wildignore+=*DS_Store*
set wildignore+=*.gem
set wildignore+=*.png,*.jpg,*.gif
set wildignore+=**/.git/**
set wildignore+=**/node_modules/**

" change default directory
let s:vim_swapfiles_dir = expand('~/.vim/.swp/')
if !isdirectory(s:vim_swapfiles_dir)
    silent! call mkdir(s:vim_swapfiles_dir, 'p')
endif
set directory=~/.vim/.swp//

" tags
set tags=./tags;,tags

" mapping
let mapleader=','
let maplocalleader='\'
nnoremap <leader>cd :cd %:p:h<CR> :pwd<CR>
nnoremap <c-g><c-g> <Esc>
cnoremap <C-G> <C-C>

" highlight Pmenu ctermbg=grey

packadd! onedark.vim
silent colorscheme onedark

runtime ftplugin/man.vim
packadd matchit
packadd cfilter
packadd! comment
packadd! helptoc

if has('patch-9.1.0598')
    set completeopt+=fuzzy
endif

if !has('nvim')
    packadd editorconfig
endif

if !empty(glob('~/.vim_local'))
    source ~/.vim_local
endif
