#!/usr/bin/env bash

set -e

rime_dir=
case "$OSTYPE" in
    linux*)
        export rime_frontend=fcitx5-rime
        rime_dir="$HOME/.local/share/fcitx5/rime"
        ;;
    darwin*)
        rime_dir="$HOME/Library/Rime"
        ;;
esac


mkdir -p "$rime_dir"

plum_dir="$rime_dir/plum"
[ ! -d "$plum_dir" ] && git clone --depth 1 https://github.com/rime/plum.git "$plum_dir"

prog="$plum_dir/rime-install"

bash "$prog" \
    prelude \
    essay \
    luna-pinyin \
    emoji \
    emoji:customize:schema=luna_pinyin \
    emoji:customize:schema=luna_pinyin_simp
