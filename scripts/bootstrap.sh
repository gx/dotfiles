#!/usr/bin/env bash

dotfiles_dir=$(cd -- "$(dirname ${BASH_SOURCE})/.." > /dev/null 2>&1; pwd -P)

mkdir -p \
    "$HOME/.config" \
    "$HOME/.local/share"

remove_and_link () {
    for var in $@
    do
        if [ -L $HOME/$var ] && [ -e $HOME/$var ] && \
            [ "$(readlink -- $HOME/$var)" = "$dotfiles_dir/$var" ]; then
            continue
        fi

        rm -v -rf $HOME/$var
        ln -v -s $dotfiles_dir/$var $HOME/$var
    done
}

link_all () {
    remove_and_link bin .aliases .macos \
        .tmux.conf .urlview .zshrc \
        .vimrc .vim .path .functions .emacs.d .gitconfig \
        .config/i3 .config/polybar .xinitrc .pam_environment \
        .newsboat .ledgerrc .nnnrc .alacritty.yml .fzfrc \
        .env .config/kitty .wezterm.lua .config/mpv \
        .config/sway .config/swaylock .config/zathura .tigrc .config/foot \
        .config/fish .arch.aliases .bashrc \
        .config/git .config/nvim \
        .p10k.zsh \
        .p10krc
        '.local/share/zsh-completions' \
        '.local/share/zsh-syntax-highlighting' \
        '.local/share/zsh-autosuggestions' \
        '.local/share/powerlevel10k'
}

main () {
    if [ $# -eq 0 ] || [ $1 = 'all' ]; then
        link_all
        return
    fi

    remove_and_link $@
}

main "$@"
