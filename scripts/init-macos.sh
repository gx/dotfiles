#!/usr/bin/env bash

brew install \
    git \
    nnn \
    fzf \
    navi \
    tmux \
    tig \
    z \
    global \
    universal-ctags

brew install --cask \
    macvim \
    flameshot
