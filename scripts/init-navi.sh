#!/usr/bin/env bash

if ! command -v navi &> /dev/null; then
    echo 'navi is not installed. Failed.'
    exit 1
fi

cheats_path="$(navi info cheats-path)/personal"

if [ ! -d "$cheats_path" ]; then
    git clone https://git.gxlin.org/navi-cheats "$cheats_path"
fi

cd "$cheats_path" && git pull --rebase
