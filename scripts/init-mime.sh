#!/usr/bin/env bash

set_xdg_mime () {
    local application=$1
    local mimetype=$2

    if [ -f /usr/share/applications/$application -o -f $HOME/.local/share/applications/$application ]
    then
        xdg-mime default $application $mimetype
    fi
}

set_xdg_mime thunar.desktop inode/directory

set_xdg_mime org.pwmt.zathura.desktop application/pdf

set_xdg_mime firefox.desktop x-scheme-handler/http
set_xdg_mime firefox.desktop x-scheme-handler/https

set_xdg_mime sxiv.desktop image/png
set_xdg_mime sxiv.desktop image/jpeg

set_xdg_mime mpv.desktop video/x-flv
set_xdg_mime mpv.desktop video/x-ms-asf
set_xdg_mime mpv.desktop video/mp4
set_xdg_mime mpv.desktop audio/mpeg
