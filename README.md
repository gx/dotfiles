Dotfiles of gx. Don't use it unless you're gx.

## Vim

Packages included is listed below.

Motion:

- [vim9-stargate](https://github.com/monkoose/vim9-stargate): modern
  alternative to easymotion.
- [vim-sneak](https://github.com/justinmk/vim-sneak): Jump to any
  location with two characters. It should be `opt`. Otherwise the config
  in `after` folder would not work.
- [hop.nvim](https://github.com/smoka7/hop.nvim): modern alternative to
  easymotion in neovim.

Navigation:

- symbols-outline.nvim: alternative to tagbar in neovim.
- cscope_maps.nvim: as neovim deprecated cscope in neovim 0.9, this
  plugin is required to use the cscope features.
- nvim-lspconfig

Languages specfic:

- [vim-javascript](https://github.com/pangloss/vim-javascript):
  Improve javascript syntax highlight.
- [vim-terraform](https://github.com/hashivim/vim-terraform):
  Improve terraform syntax highlight.
- [zk.vim](https://github.com/igxlin/zk.vim):
  Use zk to help editing markdown notes.

Editing:

- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp): Code completion in
  neovim.

  Related completion sources:

  - cmp-buffer
  - cmp-cmdline
  - cmp-nvim-lsp
  - cmp-nvim-ultisnips (kind of buggy)
  - cmp-omni
  - cmp-path

- lsp_signature.nvim

Interface:

- [diffview.nvim](https://github.com/sindrets/diffview.nvim): Help to
  diff file changes.
- [gv.vim](https://github.com/junegunn/gv.vim): Helpful plugin to view
  logs.

Misc:

- [vim-highlightedyank](https://github.com/machakann/vim-highlightedyank):
  highlight yanked text.
- nvim-treesitter

Good but not included yet:

- [emmet-vim](https://github.com/mattn/emmet-vim): help edit html.
- [quickr-preview.vim](https://github.com/ronakg/quickr-preview.vim): to
  preview the result in quickfix.
- [neomake](https://github.com/neomake/neomake): asynchronous linting
  and make framework.
- [vim-autoformat](https://github.com/vim-autoformat/vim-autoformat)
- [any-jump.vim](https://github.com/pechorin/any-jump.vim): use popup
  window to preview the definition and usage.
- [vim-signature](https://github.com/kshenoy/vim-signature): show marks.
- [context.vim](https://github.com/wellle/context.vim): automatically
  folding when reading long code. It is very cool, but the folding
  sometimes is not correct. (2023-12-04)

Good but not suitable for me:

- [vim-cool](https://github.com/romainl/vim-cool): improve hlsearch.
- [vim-grammarous](https://github.com/rhysd/vim-grammarous): use
  LanguageTool to check grammar. But I found that ALE support
  languagetool too.
- [quick-scope](https://github.com/unblevable/quick-scope): show hints
  for `f` and `F`. I found it kind of distractful.

## Sway

Packages instealled:

* sway
* swaybg
* waybar
* otf-font-awesome

