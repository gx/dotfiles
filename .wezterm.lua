local wezterm = require 'wezterm'

return {
    font = wezterm.font_with_fallback {
        'DejaVu Sans Mono',
        'Monaco',
    },
    font_size = 16,
}
