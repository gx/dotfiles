if status is-interactive
    set -g fish_greeting

    set -g __fish_git_prompt_showdirtystate 1
    set -g __fish_git_prompt_showuntrackedfiles 1
    set -g __fish_git_prompt_showcolorhints 1

    bind \cx\ce edit_command_buffer

    source "$HOME/.macos"

    command -v zoxide > /dev/null && zoxide init --cmd j fish | source
    command -v navi > /dev/null && eval "$(navi widget fish)"

    if command -v nnn > /dev/null;
        set -l function_file "$HOME/.config/fish/functions/n.fish"
        if [ ! -r $function_file ];
            echo 'Downloading nnn function'
            curl -L 'https://raw.githubusercontent.com/jarun/nnn/master/misc/quitcd/quitcd.fish' > $function_file
        end
    end

    if command -v fzf > /dev/null;
        [ -n "$IS_MACOS" ] && source "$(brew --prefix fzf)/shell/key-bindings.fish"
        function fish_user_key_bindings; fzf_key_bindings; end
    end

    source "$HOME/.aliases"

    [ -r "$HOME/.local_config.fish" ] && source "$HOME/.local_config.fish"
end
