# vim: ft=sh

export FZF_DEFAULT_COMMAND='find . -type f'
if command -v fd &> /dev/null; then
    export FZF_DEFAULT_COMMAND='fd --type f --hidden --follow \
        --exclude .git --exclude .cache'
fi

export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND
export FZF_DEFAULT_OPTS='--bind ctrl-j:jump --bind ctrl-a:select-all'

# completion and key bindings

case $SHELL in
*/zsh)
    complete_share_file=/usr/share/fzf/completion.zsh
    keybinds_share_file=/usr/share/fzf/key-bindings.zsh

    [ -f $complete_share_file ] && source $complete_share_file
    [ -f $keybinds_share_file ] && source $keybinds_share_file

    complete_homebrew_file=$HOMEBREW_PREFIX/opt/fzf/shell/completion.zsh
    keybinds_homebrew_file=$HOMEBREW_PREFIX/opt/fzf/shell/key-bindings.zsh

    [ -f $complete_homebrew_file ] && source $complete_homebrew_file
    [ -f $keybinds_homebrew_file ] && source $keybinds_homebrew_file

    ;;
*/bash)
    ;;
*)
esac

# functions

fcd () {
    local dir dirs
    if command -v fd &> /dev/null; then
        dirs=$(
            fd --type directory --hidden \
                --exclude .git --no-ignore
        )
    fi
    dir=$(echo "$dirs" | fzf)
    if [ $? -eq 0 ]; then
        cd "$dir"
    fi
}

fco () {
    local tags branches target
    branches=$(
        git --no-pager branch --all \
            --format="%(if)%(HEAD)%(then)%(else)%(if:equals=HEAD)%(refname:strip=3)%(then)%(else)%1B[0;34;1mbranch%09%1B[m%(refname:short)%(end)%(end)" \
        | sed '/^$/d'
    ) || return
    tags=$(
        git --no-pager tag | awk '{print "\x1b[35;1mtag\x1b[m\t" $1}'
    ) || return
    target=$(
        (echo $branches; echo $tags) \
        | fzf --no-hscroll --no-multi -n 2 --ansi
    ) || return
    git checkout $(awk '{print $2}' <<<$target)
}

fs () {
    local session
    session=$(tmux list-sessions -F "#{session_name}" \
        | fzf --query=$1 --select-1 --exit-0) \
        && tmux switch-client -t $session
}

tm() {
  [[ -n "$TMUX" ]] && change="switch-client" || change="attach-session"
  if [ $1 ]; then
    tmux $change -t "$1" 2>/dev/null || (tmux new-session -d -s $1 && tmux $change -t "$1"); return
  fi
  session=$(tmux list-sessions -F "#{session_name}" 2>/dev/null | fzf --exit-0) &&  tmux $change -t "$session" || echo "No sessions found."
}

if command -v jrnl > /dev/null; then
    fjrnl () {
      title=$(jrnl --short | fzf --tac --no-sort) &&
      jrnl -on "$(echo $title | cut -c 1-16)" $1
    }
fi

# fcs - get git commit sha
# example usage: git rebase -i `fcs`
fcs() {
  local commits commit
  commits=$(git log --color=always --pretty=oneline --abbrev-commit --reverse) &&
  commit=$(echo "$commits" | fzf --tac +s +m -e --ansi --reverse) &&
  echo -n $(echo "$commit" | sed "s/ .*//")
}
