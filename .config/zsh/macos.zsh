#!/usr/bin/env bash

[ "$(uname)" = Darwin ] || return

export IS_MACOS=1

export BROWSER='open'
alias bug='brew upgrade --greedy'

[ "$(spctl --status)" = 'assessments enabled' ] &&
    echo 'Software installment is limited again. Password is required to remove the limit.' &&
    sudo spctl --master-disable

# Much swifter after comment them. Maybe uncomment them after I find
# they are necessary.
[ "$(arch)" = 'arm64' ] || eval "$(/usr/local/Homebrew/bin/brew shellenv)"

if [ "$(arch)" = 'arm64' ]; then
    # Output of brew shellenv
    export HOMEBREW_PREFIX="/opt/homebrew";
    export HOMEBREW_CELLAR="/opt/homebrew/Cellar";
    export HOMEBREW_REPOSITORY="/opt/homebrew";
    fpath[1,0]="/opt/homebrew/share/zsh/site-functions";
    export PATH="/opt/homebrew/bin:/opt/homebrew/sbin${PATH+:$PATH}";
    [ -z "${MANPATH-}" ] || export MANPATH=":${MANPATH#:}";
    export INFOPATH="/opt/homebrew/share/info:${INFOPATH:-}";
fi

BREW_GNUBIN_PATH="$HOMEBREW_PREFIX/opt/coreutils/libexec/gnubin"
[ -d "$BREW_GNUBIN_PATH" ] &&
    export PATH="$BREW_GNUBIN_PATH:$PATH"

[ -d '/usr/local/opt/ruby/bin' ] && export PATH="/usr/local/opt/ruby/bin:$PATH"
[ -d '/opt/homebrew/opt/ruby/bin' ] && export PATH="/opt/homebrew/opt/ruby/bin:$PATH"

export HOMEBREW_NO_AUTO_UPDATE=1

[ -d "/usr/local/opt/coreutils/libexec/gnubin" ] &&
    export PATH="/usr/local/opt/coreutils/libexec/gnubin:$PATH"
